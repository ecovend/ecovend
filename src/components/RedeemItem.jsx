/* eslint-disable no-unused-vars */
import React from "react";
import "../styles/RedeemItem.css";

function RedeemItem(props) {
  return (
    <div className="redeemItem">
      <img src="nuts.png" alt="" />
      <div className="info">
        <p>Mezcla de Frutos Secos</p>
        <button>CANJEAR EN MÁQUINA</button>
      </div>
    </div>
  );
}

export default RedeemItem;
