/* eslint-disable react/prop-types */
import "../styles/Field.css";

function Field(props) {
  return <div className="field">
    <p>{props.text}</p>
    <input value={props.value} type={props.type} onChange={(e) => {
      props.setValue(e.target.value)
    }} />
  </div>;
}

export default Field;
