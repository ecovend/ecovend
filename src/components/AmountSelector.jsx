/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import "../styles/AmountSelector.css";

function AmountSelector() {
    let items = ["", "", 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, "", ""];
    const [index, setIndex] = useState(0);
    let startX = 0;
    let endX = 0;

    const handleTouchStart = (e) => {
        startX = e.touches[0].clientX;
    };

    const handleTouchMove = (e) => {
        endX = e.touches[0].clientX;
    };

    const handleTouchEnd = () => {
        if (startX - endX > 50 && index < items.length - 5) {
            setIndex(index + 1);
        } else if (endX - startX > 50 && index > 0) {
            setIndex(index - 1);
        }
    };

    useEffect(() => {
        console.log(items[index + 2])
    }, [index])
    

    return (
        <div className='amountSelector'
            onTouchStart={handleTouchStart}
            onTouchMove={handleTouchMove}
            onTouchEnd={handleTouchEnd}
            onMouseDown={handleTouchStart}
            onMouseMove={handleTouchMove}
            onMouseUp={handleTouchEnd}>
            <p>{items[index]}</p>
            <p>{items[index + 1]}</p>
            <p>{items[index + 2]}</p>
            <p>{items[index + 3]}</p>
            <p>{items[index + 4]}</p>
        </div>
    );
}

export default AmountSelector;
