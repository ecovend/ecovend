/* eslint-disable react/prop-types */
import "../styles/Header.css"

function Header(props) {
  const types = {
    buy: {
      name: "Compra",
      description: "Mezcla de Frutos Secos",
    },
    pay: {
      name: "Pagar",
      description: "Mezcla de Frutos Secos",
    },

    redeem: {
      name: "Canjear",
      description: "Mezcla de Frutos Secos",
    },
    profile: {
      name: "Perfil",
      description: "Mezcla de Frutos Secos",
    },
  };

  return (
    <div className="header">
      <h4>{types[props.type].name}</h4>
      <p>{types[props.type].description}</p>
    </div>
  );
}

export default Header;
