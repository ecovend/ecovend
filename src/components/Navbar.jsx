/* eslint-disable no-undef */
/* eslint-disable react/prop-types */
import "../styles/Navbar.css";
import env from "../../env";
import axios from "axios";

function Navbar(props) {
  function changeType(type) {
    props.setType(type);
  }

  function signOut() {
    localStorage.removeItem("token");
    setTimeout(() => {
      window.location.reload();
    }, 1000);
  }

  return (
    <div className="navbar">
      <div onClick={() => changeType("buy")} className="button">
        <img src="resources/buy.svg" alt="" height={30} />
        <p>Comprar</p>
      </div>
      <div onClick={() => changeType("redeem")} className="button">
        <img src="resources/redeem.svg" alt="" height={30} />
        <p>Canjear</p>
      </div>
      <div onClick={() => signOut()} className="button">
        <img src="resources/signout.svg" alt="" height={30} />
        <p>Salir</p>
      </div>
    </div>
  );
}

export default Navbar;
