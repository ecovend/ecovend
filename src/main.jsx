import React from "react";
import ReactDOM from "react-dom/client";
import App from "./Router.jsx";
import "./index.css";
import { BrowserView, MobileView } from "react-device-detect";

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <MobileView>
      <App />
    </MobileView>
    <BrowserView>
      <p>Only Mobile</p>
    </BrowserView>
  </React.StrictMode>
);
