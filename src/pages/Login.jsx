import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import "../styles/SignInUp.css";
import Field from "../components/Field";
import { useLocation } from "react-router-dom";
import axios from "axios";
import env from "../../env";

function Login() {
  const location = useLocation();
  const query = new URLSearchParams(location.search);
  const isNew = query.get("new");

  const navigate = useNavigate();

  const [id, setid] = useState("");
  const [password, setPassword] = useState("");

  const [showPopUp, setShowPopUp] = useState(false);
  const [messagePopUp, setMessagePopUp] = useState("");

  function showPopUpMessage(message) {
    setShowPopUp(true);
    setMessagePopUp(message);
  }

  useEffect(() => {
    if (isNew) {
      showPopUpMessage(
        "¡Gracias por registrarte! Ahora inicia sesión para poder comprar frutos secos."
      );
    }
  }, []);

  function redirectTo(url) {
    navigate(url);
  }

  async function login() {
    try {
      const response = await axios.post(env.BACKEND_URL + "/user/login", {
        id,
        password,
      });
      if (response.status === 200) {
        const { token } = response.data;
        console.log("Registration successful. Token:", token);
        localStorage.setItem("token", token);
        showPopUpMessage("Inicio de sesión exitoso... Redirigiendo...")
        setTimeout(() => {
          redirectTo("/")
        }, 2000)
      } else {
        console.error("Registration failed:", response.data.error);
        showPopUpMessage("Error al iniciar sesión, cédula o contraseña incorrecta.")
      }
    } catch (error) {
      console.error("Error registering user:", error);
      showPopUpMessage("Error al iniciar sesión, cédula o contraseña incorrecta.")
    }
  }

  return (
    <div className="signInUp">
      <div className="top">
        <h1>EcoVend</h1>
        <p>
          ¡Bienvenid@ de vuelta! Introduzca sus datos para ingresar a su cuenta.
        </p>

        {showPopUp ? <p className="popup">{messagePopUp}</p> : ""}

        <div className="fields">
          <Field type="number" text="Cédula" value={id} setValue={setid} />
          <Field type="password" text="Contraseña" value={password} setValue={setPassword} />
        </div>
      </div>
      <div className="bottom">
        <button onClick={() => login()}>INICIAR SESIÓN</button>
        <p>
          ¿No tienes una cuenta?{" "}
          <span onClick={() => redirectTo("/register")}>Registrarse</span>
        </p>
      </div>
    </div>
  );
}

export default Login;
