import { useNavigate } from "react-router-dom";
import "../styles/Home.css";
import { useState, useEffect } from "react";
import HomeLogged from "./HomeLogged";
import axios from "axios";
import env from "../../env";

function Home() {
  const navigate = useNavigate();

  const [logged, setLogged] = useState(false);

  function redirectTo(url) {
    navigate(url);
  }

  const [user, setUser] = useState(null)

  useEffect(() => {
    const fetchData = async () => {
      const token = localStorage.getItem('token');
      try {
        const response = await axios.get(`${env.BACKEND_URL}/user`, {
          headers: {
            'Authorization': `Bearer ${token}`
          }
        });
        if (response.status === 200) {
          setLogged(true)
          setUser(response.data.user)
        }
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };
    fetchData();
  }, []);
  

  if (!logged) {
    return (
      <div className="home">
        <div className="top">
          <h1>EcoVend</h1>
          <img src="nuts.png" alt="" height={250} />
        </div>
        <div className="bottom">
          <h2>Mezcla de Frutos Secos</h2>
          <p>
            Un snack saludable con un alto valor nutricional. Perfecto para
            cualquier momento del día.
          </p>
          <div className="interact">
            <button onClick={() => redirectTo("/register")}>REGISTRARSE</button>
            <p>
              ¿Ya tienes una cuenta?{" "}
              <span onClick={() => redirectTo("/login")}>Iniciar Sesión</span>
            </p>
          </div>
        </div>
      </div>
    );
  }
  else {
    return (
      <HomeLogged/>
    )
  }
}

export default Home;
