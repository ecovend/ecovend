import { useState } from "react";
import { useNavigate } from "react-router-dom";
import "../styles/SignInUp.css";
import Field from "../components/Field";
import axios from "axios";
import env from "../../env";

function Register() {
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [id, setid] = useState("");
  const [password, setPassword] = useState("");

  const [showPopUp, setShowPopUp] = useState(false)
  const [messagePopUp, setMessagePopUp] = useState("")

  function showPopUpMessage(message){
    setShowPopUp(true)
    setMessagePopUp(message);
  }

  function redirectTo(url) {
    navigate(url);
  }

  async function register() {
    try {
      const response = await axios.post(env.BACKEND_URL + "/user/register", {
        id,
        name,
        lastName,
        password,
      });
      if (response.status === 203) {
        showPopUpMessage(response.data.errors.join("\n"))
        return
      }
      if (response.status === 201) {
        console.log("Registration successful.");
        redirectTo("/login?new=true")
      } else {
        console.error("Registration failed:", response.data.error);
        showPopUpMessage("Error al registrar el usuario. Ya existe o tiene formato incorrecto.")
      }
    } catch (error) {
      console.error("Error registering user:", error);
      showPopUpMessage("Error al registrar el usuario. Ya existe o tiene formato incorrecto.")
    }
  }

  return (
    <div className="signInUp">
      <div className="top">
        <h1>EcoVend</h1>
        <p>
          ¡Bienvenid@! Por favor introduzca sus datos para registrar una cuenta.
        </p>

        {
          showPopUp ? <p className="popup">{messagePopUp}</p> : ""
        }

        <div className="fields">
          <Field type="text" text="Nombre" value={name} setValue={setName} />
          <Field type="text" text="Apellido" value={lastName} setValue={setLastName} />
          <Field type="number" text="Cédula" value={id} setValue={setid} />
          <Field type="text" text="Contraseña" value={password} setValue={setPassword} />
        </div>
      </div>
      <div className="bottom">
        <button onClick={() => register()}>REGISTRARSE</button>
        <p>
          ¿Ya tienes una cuenta?{" "}
          <span onClick={() => redirectTo("/login")}>Iniciar Sesión</span>
        </p>
      </div>
    </div>
  );
}

export default Register;
