import { useState } from "react";
import "../../styles/Redeem.css";
import RedeemItem from "../../components/RedeemItem";

function Redeem() {
  const [hasRedeem, setHasRedeem] = useState(true);

  const Redeems = [1, 2, 3, 4, 5, 6]

  return (
    <div className="redeem">
      <div className="top">
        <p>¿Te dieron un cupón? ¡Puedes usarlo aquí!</p>
        <div className="inputRedeem">
          <input type="text" />
          <button>CANJEAR</button>
        </div>
      </div>
      <div className="bottom">
        <p className="title">Tus Canjes</p>
        <div className="container">
          {!hasRedeem ? (
            <>
              <img className="notFoundImg" src="resources/no-redeem.png" alt="" />
              <p className="notFound">
                No tienes ningún canje comprado. Puedes comprar frutos secos en
                la sección de <br/>
                <span>Comprar</span>
              </p>
            </>
          ) : (
            <>
              {
                Redeems.map((x, key) => {
                  return (
                    <RedeemItem info={x} key={key}/>
                  )
                })
              }
            </>
          )}
        </div>
      </div>
    </div>
  );
}

export default Redeem;
