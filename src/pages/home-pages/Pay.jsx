import "../../styles/Pay.css"

function Pay() {
  return (
    <div className="pay">
        <p className="amountTitle">Monto a Pagar</p>
        <p className="amount">150.01 Bs</p>
        <p className="description">Por favor, realizar un Pago Móvil basado en la siguiente información. Una vez realizado dale al botón de abajo para checkear el pago.</p>
        <div className="info">
            <p className='title'>Banco</p>
            <p className='value'>0108 - Provincial</p>
            <p className='title'>Cédula</p>
            <p className='value'>E. 00.000.000</p>
            <p className='title'>Teléfono</p>
            <p className='value'>0412-0000-000</p>
        </div>
        <p className='important'><span>Importante</span>: El monto debe ser exacto, con los decimales incluidos, de otro modo, la transacción no podrá ser procesada</p>
        <button>¡YA HE PAGADO!</button>
        <p className="cancel">¿Te equivocaste en algún detalle? <span>Cancelar Pago</span></p>
    </div>
  )
}

export default Pay