import AmountSelector from "../../components/AmountSelector";
import "../../styles/Buy.css";

function Buy() {
  return (
    <div className="buy">
      <div className="top">
        <div className="showcase">
          <p>&#60;</p>
          <img src="nuts.png" alt=""/>
          <p>&#62;</p>
        </div>
        <p>. . . .</p>
      </div>
      <div className="bottom">
        <p className="priceTitle">Precio</p>
        <p className="price">50 Bs</p>
        <p className="description">Nuestra Mezcla de Frutos Secos combina almendras, nueces, avellanas y pistachos de alta calidad. Ofrece una buena fuente de proteínas, fibra y grasas saludables.</p>
        <div className="amount">
            <p className="amountTitle">Cantidad a comprar</p>
            <AmountSelector/>
        </div>
        <p className="total">Monto a pagar <span>150 Bs</span></p>
        <button>PROCEDER A PAGAR</button>
      </div>
    </div>
  );
}

export default Buy;
