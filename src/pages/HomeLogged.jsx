import { useState } from "react";
import Navbar from "../components/Navbar";
import "../styles/HomeLogged.css";
import Header from "../components/Header";
import Buy from "./home-pages/Buy";
import Pay from "./home-pages/Pay";
import Redeem from "./home-pages/Redeem";

function HomeLogged() {
  const [type, setType] = useState("pay");

  return (
    <div className="homeLogged">
      <Header type={type} />
      {type === "buy" ? <Buy /> : ""}
      {type === "pay" ? <Pay /> : ""}
      {type === "redeem" ? <Redeem/> : ""}
      <Navbar setType={setType} />
    </div>
  );
}

export default HomeLogged;